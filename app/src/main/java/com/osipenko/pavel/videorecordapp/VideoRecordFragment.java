package com.osipenko.pavel.videorecordapp;

import android.annotation.TargetApi;
import android.app.Activity;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class VideoRecordFragment extends Fragment {

    public static final String TAG = "VideoRecordFragment";

    private static final SparseIntArray ORIENTATIONS_BACK = new SparseIntArray();

    static {
        ORIENTATIONS_BACK.append(Surface.ROTATION_0, 90);
        ORIENTATIONS_BACK.append(Surface.ROTATION_90, 0);
        ORIENTATIONS_BACK.append(Surface.ROTATION_180, 270);
        ORIENTATIONS_BACK.append(Surface.ROTATION_270, 180);
    }

    private static final SparseIntArray ORIENTATIONS_FRONT = new SparseIntArray();

    static {
        ORIENTATIONS_FRONT.append(Surface.ROTATION_0, 0);
        ORIENTATIONS_FRONT.append(Surface.ROTATION_90, 90);
        ORIENTATIONS_FRONT.append(Surface.ROTATION_180, 180);
        ORIENTATIONS_FRONT.append(Surface.ROTATION_270, 270);
    }

    private Camera mCamera;
    private MediaRecorder mRecorder;
    private CameraPreview mCameraPreview;

    private Button mRecordButton;

    private boolean mRecording = false;

    public static VideoRecordFragment newInstance() {
        VideoRecordFragment fragment = new VideoRecordFragment();
        fragment.setRetainInstance(true);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    @SuppressWarnings("deprecation")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_video_record, container, false);

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mRecordButton = (Button) view.findViewById(R.id.button_record);
        mRecordButton.setText("Start");
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check if external storage is usable
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    Log.i(TAG, "Button 'Start' pressed");
                    updateRecordingState();
                } else {
                    Toast.makeText(getActivity(), "Cannot use storage.", Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.surface_video);
        mCameraPreview = new CameraPreview(getActivity(), mCamera, 0);
        layout.addView(mCameraPreview, 0);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mCamera = openCamera();
        if (mCamera == null) {
            Toast.makeText(getActivity(), "Opening camera failed", Toast.LENGTH_LONG).show();
            getActivity().finish();
        }

        setImprovements();

        mCameraPreview.refreshCamera(mCamera);
    }

    @TargetApi(15)
    private void setImprovements() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            Camera.Parameters parameters = mCamera.getParameters();
            if (parameters.isVideoStabilizationSupported()) {
                parameters.setVideoStabilization(true);
            }
            if (parameters.isAutoWhiteBalanceLockSupported()) {
                parameters.setAutoWhiteBalanceLock(true);
            }
        }
    }

    @SuppressWarnings("deprecation")
    private Camera openCamera() {
        Camera camera = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            camera = Camera.open();
        } else {
            if (Camera.getNumberOfCameras() > 0) {
                try {
                    camera = Camera.open(0);
                } catch (RuntimeException re) {
                    Log.e(TAG, "Opening the camera fails" + re.toString());
                }
            }
        }
        return camera;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    /*
     * Initialize the camera and recorder.
     * The order of these methods are important because MediaRecorder is a fairly
     * strict state machine that moves through states as the methods are called.
     */
    @SuppressWarnings("deprecation")
    private void initializeRecorder() throws IllegalStateException, IOException {
        final Activity activity = getActivity();
        if (null == activity) {
            return;
        }
        Log.i(TAG, "Method 'initializeRecorder' started");

        mRecorder = new MediaRecorder();

        //Unlock the camera to let MediaRecorder use it
        mCamera.unlock();
        mRecorder.setCamera(mCamera);
        //Update the source settings
        mRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        //Update the output settings
        //Create a new directory on external storage
        File rootPath = new File(Environment.getExternalStorageDirectory(), "MyVideoRecords");
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        String currentDate = dateFormat.format(new Date());
        String videoFile = "recorded_video_" + currentDate + ".mp4";

        //Create the file reference
        File recordOutput = new File(rootPath, videoFile);
        if (recordOutput.exists()) {
            recordOutput.delete();
        }
        Log.i(TAG, "Method 'initializeRecorder' - Create the file reference");
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            mRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
        } else {
            mRecorder.setProfile(CamcorderProfile.get(0, CamcorderProfile.QUALITY_HIGH));
        }
        mRecorder.setOutputFile(recordOutput.getAbsolutePath());

        setMediaRecordOrientation(activity, 0);

        //Optionally, set limit values on recording
        mRecorder.setMaxDuration(30000); // 30 seconds
        mRecorder.setMaxFileSize(10000000); // Approximately 10 megabytes

        mRecorder.prepare();
        Log.i(TAG, "Method 'initializeRecorder' completed");
    }

    private void updateRecordingState() {
        if (mRecording) {
            mRecording = false;
            //Reset the recorder state for the next recording
            mRecorder.stop();
            mRecorder.reset();
            Toast.makeText(getActivity(), "Video captured!", Toast.LENGTH_SHORT).show();
            //Take the camera back to let preview continue
            mCamera.lock();
            mRecordButton.setText("Start");
        } else {
            try {
                //Reset the recorder for the next session
                initializeRecorder();
                //Start recording
                mRecording = true;
                mRecorder.start();
                mRecordButton.setText("Stop");
            } catch (Exception e) {
                //Error occurred initializing recorder
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    @TargetApi(9)
    private void setMediaRecordOrientation(Activity activity, int cameraId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
            mRecorder.setOrientationHint(ORIENTATIONS_BACK.get(rotation));
        } else {
            mRecorder.setOrientationHint(ORIENTATIONS_FRONT.get(rotation));
        }
    }

}
