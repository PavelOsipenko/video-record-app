package com.osipenko.pavel.videorecordapp;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;


public class VideoRecordActivity extends AppCompatActivity {

    private final String VIDEO_FRAGMENT_TAG = "VFTAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_record);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
                    .show();
            finish();
        }

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(VIDEO_FRAGMENT_TAG);

        if (null == fragment) {
            fragment = VideoRecordFragment.newInstance();
            manager.beginTransaction()
                    .add(R.id.container, fragment, VIDEO_FRAGMENT_TAG)
                    .commit();
        }
    }

}
