# Пояснения к проекту  #

## Данное тестовое приложение предназначено для записи видео на устройствах Android

### Требования к устройствам ###

* min. API 8
* наличие задней или фронтальной камеры
* возможность сохранения файлов во внешнем хранилище устройства


### Особенности данного проекта ###

* При написании был использован только класс android.hardware.Camera 
* Видеофайлы сохраняются в папке MyVideoRecords внешнего хранилища
* Каждый видеофайл при записи получает уникальное имя
* По возможности поддерживается горизонтальная ориентация записываемого видеоматериала